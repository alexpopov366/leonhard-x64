/*
 * gpc_io_host.h
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef MQ_H_
#define MQ_H_

#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include <thread>
#include <mutex>
#include "gpc_host.h"

//#define DEBUG_INFO

//====================================
// Адресация ресурсов в Global Memory
//====================================
#define MSG_BLOCK_START 0x00010000                  			// area for IO

// Check parameters to avoid global memory conflict
// 1. MSG_BUFFER_SIZE == 2 * (MSG_QUEUE_FSIZE + MSG_BUFFER_SIZE)
// 2. MSG_BLOCK_SIZE * (GPC cores in GROUP, 6 is default) < 64K
#define MSG_BLOCK_SIZE	0x2800									// 10K Block size for every GPC
#define MSG_QUEUE_FSIZE 0x400                      				// 1024 bytes for queues
#define MSG_BUFFER_SIZE	0x1000									// 4096 bytes buffers for GPC IO

// Automatically calculated parameters
#define MSG_QUEUE_SIZE  (MSG_QUEUE_FSIZE - 2 * sizeof(int))  	// 1016 bytes for data, 8 bytes for pointers
#define HOST2GPC_QUEUE  0                           			// Offset for Host2GPC queue
#define GPC2HOST_QUEUE  MSG_QUEUE_FSIZE             			// Offset for GPC2Host queue
#define HOST2GPC_BUFFER (2*MSG_QUEUE_FSIZE)                     // Offset for Host2GPC queue
#define GPC2HOST_BUFFER (HOST2GPC_BUFFER + MSG_BUFFER_SIZE)		// Offset for GPC2Host queue


//=============================================
// Класс для взаимодействия с GPC через очереди
//=============================================

class gpc_io : public gpc
{
private:
  // Queue base offsets
  unsigned int gpc2host_queue;
  unsigned int host2gpc_queue;
  // gpn2host queue pointers
  unsigned int gpc2host_head_offset; // updated by gpc
  unsigned int gpc2host_tail_offset; // updated by host
  // host2gpn queue pointers
  unsigned int host2gpc_head_offset; // updated by host
  unsigned int host2gpc_tail_offset; // updated by gpc  
  // gpn2host queue pointers
  unsigned int gpc2host_head;
  unsigned int gpc2host_tail;
  // host2gpn queue pointers
  unsigned int host2gpc_head;
  unsigned int host2gpc_tail;
  // Buffers offsets
  unsigned int gpc2host_buffer;
  unsigned int host2gpc_buffer;
  // Buffer for IO
  cl_uint host2gpc_buf[1];
  cl_uint gpc2host_buf[1];
  // Threads and alive flags
  std::thread *mq_send_th;
  std::mutex mq_send_th_alive;
  std::thread *mq_receive_th;
  std::mutex mq_receive_th_alive;
  std::thread *buf_write_th;
  std::mutex buf_write_th_alive;
  std::thread *buf_read_th;
  std::mutex buf_read_th_alive;
  // Read queue status from global memory
  void update_host2gpc_queue_status();
  void update_gpc2host_queue_status();
  // Send message to GPC queues
  void mq_send_thread(unsigned int message);
  void mq_send_buf_thread(unsigned int size, unsigned int *buffer);
  // Wait and read single message from GPC queue
  unsigned int mq_receive_thread();
  void mq_receive_buf_thread(unsigned int size, unsigned int *buffer);
  // Write data to HOST2GPC buffer
  void buf_write_thread(unsigned int size, unsigned int *buffer);
  // Read data from GPC2HOST buffer
  void buf_read_thread(unsigned int size, unsigned int *buffer);
public:
  // Constructor
  gpc_io(
		  	  cl_program 	program,
			  cl_context 	device_context,
			  cl_device_id  device_id,
			  unsigned int 	group_number,
			  unsigned int 	core_number
			  );
  // Destructor
  ~gpc_io();
  // Send message to GPC queues
  void mq_send(unsigned int message);
  void mq_send(unsigned int size, unsigned int *buffer);
  void mq_send_join();
  // Wait and read single message from GPC queue
  unsigned int mq_receive();
  void mq_receive(unsigned int size, unsigned int *buffer);
  void mq_receive_join();
  // Write data to HOST2GPC buffer
  void buf_write(unsigned int size, unsigned int *buffer);
  void buf_write_join();
  // Read data from GPC2HOST buffer
  void buf_read(unsigned int size, unsigned int *buffer);
  void buf_read_join();
    // Sync point with GPC
  void sync_with_gpc();

protected:

};

#endif
