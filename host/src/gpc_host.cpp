/*
 * gpc_host.cpp
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

 #include "gpc_host.h"

// Constructor
gpc::gpc(cl_program program, cl_context device_context, cl_device_id  device_id, unsigned int group_number,unsigned int core_number)
{
	group=group_number;
	core=core_number;
	command_queue = clCreateCommandQueue(device_context, device_id, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
	kernel = clCreateKernel(program, LNH_CORE_DEFS.KERNEL_NAME[group][core], &err);
};

gpc::~gpc() {

}

cl_uint gpc::load_file_to_memory(const char *filename, char **result)
{
	cl_uint size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)
	{
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*result = (char *)malloc(size + 1);
	if (size != fread(*result, sizeof(char), size, f))
	{
		free(*result);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*result)[size] = 0;
	return size;
}


// Load RV kernel binary to the GPN
void gpc::load_sw_kernel(char *gpcbin,cl_mem global_memory_ptr, cl_mem ddr_bus_ptr)
{
	cl_uint *gpc_kernel;
	global_memory_pointer = global_memory_ptr;
	ddr_memory_pointer = ddr_bus_ptr;
	//Set kernel arguments
	clSetKernelArg(kernel, 0, sizeof(cl_uchar), &LNH_CORE_DEFS.LEONHARD_CONFIG[group][core]);
	clSetKernelArg(kernel, 1, sizeof(cl_uchar), &GPC_RESET_LOW);
	cl_uint d_gpc_config = 0;
	clSetKernelArg(kernel, 2, sizeof(cl_uint), &d_gpc_config);
	clSetKernelArg(kernel, 3, sizeof(cl_mem), &ddr_memory_pointer); //not used
	clSetKernelArg(kernel, 4, sizeof(cl_mem), &global_memory_pointer); //global memory
	// Read gpc kernel from file
	cl_uint n_gpc0 = load_file_to_memory(gpcbin, (char **)&gpc_kernel);
	// Activate GPN RESET
	clSetKernelArg(kernel, 1, sizeof(cl_uchar), &GPC_RESET_HIGH);
	// Set kernel size
	clSetKernelArg(kernel, 2, sizeof(cl_uint), &n_gpc0);
	// Send gpn kernel to the Leonhard global memory
	clEnqueueWriteBuffer(command_queue, global_memory_pointer, CL_TRUE, 0, n_gpc0, gpc_kernel, 0, NULL, NULL);
	clFinish(command_queue);
	// Activate GPC RESET
	clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
	clFinish(command_queue);
	clSetKernelArg(kernel, 1, sizeof(cl_uchar), &GPC_RESET_LOW);
	free(gpc_kernel);
}


void gpc::start_sync(const unsigned int *event_handler)
{
	clSetKernelArg(kernel, 2, sizeof(cl_uint), event_handler);
	clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
	clFinish(command_queue);
}

void gpc::start_async(const unsigned int *event_handler)
{
	clSetKernelArg(kernel, 2, sizeof(cl_uint), event_handler);
	clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
}

void gpc::finish()
{
	clFinish(command_queue);
}



