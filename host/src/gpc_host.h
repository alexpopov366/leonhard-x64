/*
 * gpc_host.h
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef GPC_H_
#define GPC_H_

#include <stdio.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "gpc_handlers.h"
#include "gpc_defs.h"

//=============================================
// Класс для управления Graph Processing Core
//=============================================

class gpc
{
protected:
  // Descriptors
  cl_command_queue command_queue;
  cl_mem global_memory_pointer;
  cl_mem ddr_memory_pointer;
  // Core defines
  unsigned int group;
  unsigned int core;
  cl_event readevent;
  cl_event writeevent;
  cl_int err;
  cl_uint load_file_to_memory(const char *filename, char **result);

public:
  cl_kernel kernel;
  // Constructor
  gpc(
		  cl_program 	program,
		  cl_context 	device_context,
		  cl_device_id  device_id,
		  unsigned int 	group_number,
		  unsigned int 	core_number
		  );
  // Destructor
  ~gpc();
  void load_sw_kernel(char *gpnbin,cl_mem global_memory_ptr, cl_mem ddr_bus_ptr);
  void start_sync(const unsigned int *event_handler);
  void start_async(const unsigned int *event_handler);
  void finish();

protected:
};

#endif
