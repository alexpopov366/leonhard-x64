/*
 * leonhardx64.h
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#ifndef LEONHARDx64_H_
#define LEONHARDx64_H_

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "gpc_io_host.h"
#include "gpc_defs.h"

//====================================================
// Класс для управления картой ускорителя Leonhard x64
//====================================================

class leonhardx64
{
private:
	cl_int err; 														 // error code returned from api calls
	cl_uint check_status = 0; 											 // initialization errors
	cl_platform_id platform;                                             // platform id
	cl_device_id device;                                                 // compute device id
	cl_context context;                                                  // compute context
	cl_command_queue commands[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP]; // compute command queue
	cl_program program;                                                  // compute programs
	cl_mem global_memory_ptr[LNH_GROUPS_COUNT];                          // device memory used for a gpc IO
	cl_mem ddr4_bus_ptr[LNH_GROUPS_COUNT];                               // device memory used for a structures
	cl_uint load_file_to_memory(const char *filename, char **result);	 //load file to buffer
    void device_reset();
public:
  //GPC instances
  gpc_io *gpc[LNH_GROUPS_COUNT][LNH_MAX_CORES_IN_GROUP]; 		 // global memory queue
  // Constructor
  leonhardx64(
			  cl_platform_id 	platform,
			  cl_context 		device_context,
			  cl_device_id  	device_id,
			  char 				*xclbin
			  );
  // Destructor
  ~leonhardx64();
  // Reset device
  void load_sw_kernel(char *gpnbin, unsigned int group, unsigned int core);
protected:
};

#endif
