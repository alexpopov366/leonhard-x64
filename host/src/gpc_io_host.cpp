/*
 * gpc_io_host.cpp
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include "gpc_io_host.h"

// Read queue status from global memory

void gpc_io::update_host2gpc_queue_status()
{
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, host2gpc_head_offset, sizeof(cl_uint), (cl_uint*)&host2gpc_head, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, host2gpc_tail_offset, sizeof(cl_uint), (cl_uint*)&host2gpc_tail, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
}

void gpc_io::update_gpc2host_queue_status()
{
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, gpc2host_head_offset, sizeof(cl_uint), (cl_uint*)&gpc2host_head, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, gpc2host_tail_offset, sizeof(cl_uint), (cl_uint*)&gpc2host_tail, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
}

// Constructor
gpc_io::gpc_io(
		cl_program 		program,
		cl_context 		device_context,
		cl_device_id  	device_id,
		unsigned int 	group_number,
		unsigned int 	core_number
) : gpc (
		program,
		device_context,
		device_id,
		group_number,
		core_number
)
{
	// Init queues offsets
	gpc2host_queue = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE;
	host2gpc_queue = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE;
	gpc2host_head_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 8;
	gpc2host_tail_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 4;
	host2gpc_head_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 8;
	host2gpc_tail_offset = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 4;
	// Init buffers offsets
	host2gpc_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_BUFFER;
	gpc2host_buffer = MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_BUFFER;
}

// Constructor
gpc_io::~gpc_io() {
	free(mq_send_th);
	free(buf_write_th);
	free(mq_receive_th);
	free(buf_read_th);
}
//Thread version

// Send message to gpc
void gpc_io::mq_send_thread(unsigned int message)
{
	host2gpc_buf[0] = message;
	do	{ update_host2gpc_queue_status();
	} while ((host2gpc_tail + sizeof(unsigned int)) % MSG_QUEUE_SIZE == host2gpc_head); // Queue is full
	clEnqueueWriteBuffer(command_queue, global_memory_pointer, CL_TRUE, host2gpc_queue + host2gpc_tail, sizeof(cl_uint), host2gpc_buf, 0, NULL, &writeevent);
	//clWaitForEvents(1, &writeevent);
	host2gpc_tail = (host2gpc_tail + sizeof(unsigned int)) % MSG_QUEUE_SIZE;
	clEnqueueWriteBuffer(command_queue, global_memory_pointer, CL_TRUE, host2gpc_tail_offset, sizeof(cl_uint), (cl_uint *)&host2gpc_tail, 0, NULL, &writeevent);
	//clWaitForEvents(1, &writeevent);
}


// Send multiple messages to gpc
void gpc_io::mq_send_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i++) {
		mq_send_thread(buffer[i]);
	}
}

// Wait and read single message from gpc
unsigned int gpc_io::mq_receive_thread()
{
	do	{ update_gpc2host_queue_status();
	} while (gpc2host_tail == gpc2host_head); // Queue is empty
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, gpc2host_queue + gpc2host_head, sizeof(cl_uint), gpc2host_buf, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
	gpc2host_head = (gpc2host_head + sizeof(unsigned int)) % MSG_QUEUE_SIZE;
	clEnqueueWriteBuffer(command_queue, global_memory_pointer, CL_TRUE, gpc2host_head_offset, sizeof(cl_uint), (cl_uint *)&gpc2host_head, 0, NULL, &writeevent);
	//clWaitForEvents(1, &writeevent);
	return gpc2host_buf[0];
}

// Wait and read multiple messages from gpc
void gpc_io::mq_receive_buf_thread(unsigned int size, unsigned int *buffer)
{
	for (unsigned int i=0; i<size; i++) {
		buffer[i] = mq_receive_thread();
	}
}

// Write data to HOST2GPC buffer
void gpc_io::buf_write_thread(unsigned int size, unsigned int *buffer) {
	clEnqueueWriteBuffer(command_queue, global_memory_pointer, CL_TRUE, host2gpc_buffer, size, (cl_uint *)buffer, 0, NULL, &writeevent);
	clWaitForEvents(1, &writeevent);
}

// Read data from GPC2HOST buffer
void gpc_io::buf_read_thread(unsigned int size, unsigned int *buffer) {
	clEnqueueReadBuffer(command_queue, global_memory_pointer, CL_TRUE, gpc2host_buffer, size, (cl_uint *)buffer, 0, NULL, &readevent);
	clWaitForEvents(1, &readevent);
}

// Send single message to gpc
void gpc_io::mq_send(unsigned int message)
{
	mq_send_th_alive.lock();
	mq_send_thread(message);
	mq_send_th_alive.unlock();
}

// Send multiple messages to gpc asynchronously
void gpc_io::mq_send(unsigned int size, unsigned int *buffer)
{
	mq_send_th_alive.lock();
    mq_send_th = new std::thread(&gpc_io::mq_send_buf_thread, this,  size, buffer);
	mq_send_th_alive.unlock();
}

// Wait for the
void gpc_io::mq_send_join()
{
	mq_send_th->join();
}

// Wait and read single message from gpc
unsigned int gpc_io::mq_receive()
{
	mq_receive_th_alive.lock();
	unsigned int message  = mq_receive_thread();
	mq_receive_th_alive.unlock();
	return message;
}

void gpc_io::mq_receive(unsigned int size, unsigned int *buffer)
{
	mq_receive_th_alive.lock();
    mq_receive_th = new std::thread(&gpc_io::mq_receive_buf_thread, this,  size, buffer);
	mq_receive_th_alive.unlock();
}

void gpc_io::mq_receive_join()
{
	mq_receive_th->join();
}

// Write data to HOST2GPC buffer
void gpc_io::buf_write(unsigned int size, unsigned int *buffer)
{
	buf_write_th_alive.lock();
    buf_write_th = new std::thread(&gpc_io::buf_write_thread, this,  size, buffer);
	buf_write_th_alive.unlock();
}

void gpc_io::buf_write_join()
{
	buf_write_th->join();
}

// Read data from GPC2HOST buffer
void gpc_io::buf_read(unsigned int size, unsigned int *buffer)
{
	buf_read_th_alive.lock();
    buf_read_th = new std::thread(&gpc_io::buf_read_thread, this,  size, buffer);
	buf_read_th_alive.unlock();
}

void gpc_io::buf_read_join()
{
	buf_read_th->join();
}

// Syncronization point with gpc
void gpc_io::sync_with_gpc()
{
	mq_send(0);
	mq_receive();
}
