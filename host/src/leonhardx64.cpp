/*
 * leonhardx64.cpp
 *
 * host library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */
 #include "leonhardx64.h"

// Constructor
leonhardx64::leonhardx64(
		  	  cl_platform_id 	platform,
			  cl_context 		device_context,
			  cl_device_id  	device_id,
			  char 				*xclbin
			  ) {

	context = device_context;
	device = device_id;

	// Create Program Objects
	// Load binary from disk
	unsigned char *kernelbinary;
	cl_int status;

#ifdef DEBUG_INFO
	printf("INFO: loading xclbin %s\n", xclbin);
#endif

	cl_uint n_i0 = load_file_to_memory(xclbin, (char **)&kernelbinary);

#ifdef DEBUG_INFO
	if (n_i0 < 0)
	{
		printf("ERROR: failed to load kernel from xclbin: %s\n", xclbin);
		printf("ERROR: Test failed\n");
	}
#endif

	size_t n0 = n_i0;
	// Create the compute program from offline
	program = clCreateProgramWithBinary(context, 1, &device, &n0,
			(const unsigned char **)&kernelbinary, &status, &err);
	free(kernelbinary);

#ifdef DEBUG_INFO
	if ((!program) || (err != CL_SUCCESS))
	{
		printf("ERROR: Failed to create compute program from binary %d!\n", err);
		printf("ERROR: Test failed\n");
	}
#endif

	// Build the program executable
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

#ifdef DEBUG_INFO
	if (err != CL_SUCCESS)
	{
		size_t len;
		char buffer[2048];

		printf("ERROR: Failed to build program executable!\n");
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
		printf("%s\n", buffer);
		printf("ERROR: Test failed\n");
	}
#endif

	//Initialize gpc instances
	__foreach_core(group, core)
	{
		gpc[group][core] = new gpc_io(program,context,device,group,core);
	}

	// Create structs to define memory bank mapping
	cl_mem_ext_ptr_t mem_ext[LNH_GROUPS_COUNT];
	__foreach_group(group)
	{
		mem_ext[group].obj = NULL;
		mem_ext[group].param = gpc[group][LNH_CORES_LOW[0]]->kernel;

		// This buffer is not used
		mem_ext[group].flags = 3;
		ddr4_bus_ptr[group] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX, sizeof(cl_uint) * GLOBAL_MEMORY_MAX_LENGTH, &mem_ext[group], &err);

#ifdef DEBUG_INFO
		if (err != CL_SUCCESS)
		{
		  std::cout << "Return code for clCreateBuffer flags=" << mem_ext[group].flags << ": " << err << std::endl;
		}
#endif

		// Create buffer on the device to operate with Global Memory
		mem_ext[group].flags = 4;
		global_memory_ptr[group] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_EXT_PTR_XILINX, sizeof(cl_uint) * GLOBAL_MEMORY_MAX_LENGTH, &mem_ext[group], &err);

#ifdef DEBUG_INFO
		// Check either both pointers are valid
		if (!(global_memory_ptr[group] && ddr4_bus_ptr[group]))
		{
			printf("ERROR: Failed to allocate device memory!\n");
			printf("ERROR: Test failed\n");
		}
#endif

	}
}

void leonhardx64::load_sw_kernel(char *gpnbin, unsigned int group, unsigned int core) {
	// GPC queue initialization
		gpc[group][core]->load_sw_kernel(gpnbin,global_memory_ptr[group],ddr4_bus_ptr[group]);
}

// Destructor
leonhardx64::~leonhardx64() {
	device_reset();
	__foreach_group(group)
	{
		clReleaseMemObject(global_memory_ptr[group]);
		clReleaseMemObject(ddr4_bus_ptr[group]);
	}
	__foreach_core(group, core)
	{
		free(gpc[group][core]);
	}
}

//Reset device
void leonhardx64::device_reset() {

}

cl_uint leonhardx64::load_file_to_memory(const char *filename, char **result)
{
	cl_uint size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)
	{
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*result = (char *)malloc(size + 1);
	if (size != fread(*result, sizeof(char), size, f))
	{
		free(*result);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*result)[size] = 0;
	return size;
}
