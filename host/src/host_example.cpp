#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <thread>
#ifdef _WINDOWS
#include <io.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <CL/cl_ext.h>
#include "gpc_handlers.h"
#include "gpc_defs.h"
#include "leonhardx64.h"


////////////////////////////////////////////////////////////////////////////////

#define NUM_WORKGROUPS (1)
#define WORKGROUP_SIZE (256)
#define MEM_ALIGNMENT 4096
#if defined(VITIS_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg) #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(VITIS_PLATFORM)
#endif
float LNH_CLOCKS_PER_SEC;

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{

	cl_int err; // error code returned from api calls
	cl_uint check_status = 0;
	unsigned int cores_count = 0;
	__foreach_core(group, core) cores_count++;


	cl_platform_id platform_id;                                          // platform id
	cl_device_id device_id;                                              // compute device id
	cl_context context;                                                  // compute context
	char cl_platform_vendor[1001];
	char target_device_name[1001] = TARGET_DEVICE;

	if (argc != 3)
	{
		printf("Usage: %s xclbin gpnbin\n", argv[0]);
		return EXIT_FAILURE;
	}

	// Get all platforms and then select Xilinx platform
	cl_platform_id platforms[16]; // platform id
	cl_uint platform_count;
	cl_uint platform_found = 0;
	err = clGetPlatformIDs(16, platforms, &platform_count);
	if (err != CL_SUCCESS)
	{
		printf("ERROR: Failed to find an OpenCL platform!\n");
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}
	printf("INFO: Found %d platforms\n", platform_count);

	// Find Xilinx Plaftorm
	for (cl_uint iplat = 0; iplat < platform_count; iplat++)
	{
		err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *)cl_platform_vendor, NULL);
		if (err != CL_SUCCESS)
		{
			printf("ERROR: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
			printf("ERROR: Test failed\n");
			return EXIT_FAILURE;
		}
		if (strcmp(cl_platform_vendor, "Xilinx") == 0)
		{
			printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
			platform_id = platforms[iplat];
			platform_found = 1;
		}
	}
	if (!platform_found)
	{
		printf("ERROR: Platform Xilinx not found. Exit.\n");
		return EXIT_FAILURE;
	}

	// Get Accelerator compute device
	cl_uint num_devices;
	cl_uint device_found = 0;
	cl_device_id devices[16]; // compute device id
	char cl_device_name[1001];
	err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &num_devices);
	printf("INFO: Found %d devices\n", num_devices);
	if (err != CL_SUCCESS)
	{
		printf("ERROR: Failed to create a device group!\n");
		printf("ERROR: Test failed\n");
		return -1;
	}

	// iterate all devices to select the target device.
	for (cl_uint i = 0; i < num_devices; i++)
	{
		err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name, 0);
		if (err != CL_SUCCESS)
		{
			printf("ERROR: Failed to get device name for device %d!\n", i);
			printf("ERROR: Test failed\n");
			return EXIT_FAILURE;
		}
		printf("CL_DEVICE_NAME %s\n", cl_device_name);
		if (strcmp(cl_device_name, target_device_name) == 0)
		{
			device_id = devices[i];
			device_found = 1;
			printf("Selected %s as the target device\n", cl_device_name);
		}
	}

	if (!device_found)
	{
		printf("ERROR:Target device %s not found. Exit.\n", target_device_name);
		return EXIT_FAILURE;
	}

	// Create a compute context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context)
	{
		printf("ERROR: Failed to create a compute context!\n");
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}

	/*
	 *
	 * Leonhard x64 instance
	 *
	 */
	leonhardx64 *lnh_inst = new leonhardx64(platform_id,context,device_id,argv[1]);

	/*
	 *
	 * GPC sw_kernel initialization
	 *
	 */
	__foreach_core(group, core)
		lnh_inst->load_sw_kernel(argv[2], group, core);

	/*
	 *
	 * GPC frequency measurement for the first kernel
	 *
	 */
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->start_async(&__event__(frequency_measurement));
	// Measurement Body
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	sleep(1);
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->sync_with_gpc(); // Start measurement
	// End Body
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->finish();
	LNH_CLOCKS_PER_SEC = (float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive();
	printf("Leonhard clock frequency (LNH_CF):     %.1f MHz\n", LNH_CLOCKS_PER_SEC / 1000000);

	/*
	 *
	 * SW Kernel Version and Status
	 *
	 */
	__foreach_core(group, core)
	{
		printf("Group #%d \tCore #%d\n", group, core);
		lnh_inst->gpc[group][core]->start_async(&__event__(get_version));
		printf("\tSoftware Kernel Version:\t0x%08x\n", lnh_inst->gpc[group][core]->mq_receive());
		lnh_inst->gpc[group][core]->start_async(&__event__(get_lnh_status_high));
		printf("\tLeonhard Status Register:\t0x%08x", lnh_inst->gpc[group][core]->mq_receive());
		lnh_inst->gpc[group][core]->start_async(&__event__(get_lnh_status_low));
		printf("_%08x\n", lnh_inst->gpc[group][core]->mq_receive());
	}


	//-------------------------------------------------------------
	// Измерение производительности Leonhard
	//-------------------------------------------------------------

	float interval;
	char buf[100];
	err = 0;

	time_t now = time(0);
	strftime(buf, 100, "Start at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));

	printf("DISC system speed test v3.0\n%s\n\n", buf);
	printf("Test                                    value (units)\n");

	/*
	 *
	 * Memory write rate
	 *
	 */
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->start_sync(&__event__(test0));
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	printf("Average RISCV memory write rate (AMWR64): %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Bus write rate
	 *
	 */
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->start_sync(&__event__(test1));
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / LNH_CLOCKS_PER_SEC;
	printf("Average RISCV AXL bus write rate (ABWR64):%.1f (ops./sec.)\n", (float)1000000 / interval);


	/*
	 *
	 * Queue messaging rate
	 *
	 */

	clock_t start,stop;

	unsigned int *host2gpc_buffer = (unsigned int*) malloc(1024*sizeof(int));
	for (int i=0;i<1024;i++) host2gpc_buffer[i] = i;
	unsigned int *gpc2host_buffer = (unsigned int*) malloc(1024*sizeof(int));
	for (int i=0;i<1024;i++) gpc2host_buffer[i] = 0;
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->start_async(&__event__(mq_rate_test));
	start=clock();
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_send(1024,host2gpc_buffer);
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive(1024,gpc2host_buffer);
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_send_join();
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive_join();
	stop=clock();
	free(host2gpc_buffer);
	free(gpc2host_buffer);
	printf("Average Message Queue Send-Receive Rate:\t%.1f (messages/sec.)\n", float(1024 * CLOCKS_PER_SEC) / float(stop-start));

	/*
	 *
	 * Queue messaging rate
	 *
	 */

	host2gpc_buffer = (unsigned int*) malloc(1024*sizeof(int));
	for (int i=0;i<1024;i++) host2gpc_buffer[i] = i;
	gpc2host_buffer = (unsigned int*) malloc(1024*sizeof(int));
	for (int i=0;i<1024;i++) gpc2host_buffer[i] = 0;
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->start_async(&__event__(buf_rate_test));
	start=clock();
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->buf_write(1024*sizeof(int),host2gpc_buffer);
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->buf_write_join();
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_send(1024*sizeof(int));
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive();
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->buf_read(1024*sizeof(int),gpc2host_buffer);
	lnh_inst->gpc[0][LNH_CORES_LOW[0]]->buf_read_join();
	stop=clock();
	free(host2gpc_buffer);
	free(gpc2host_buffer);
	printf("Average BufferIO Send-Receive Rate:\t%.1f (bytes/sec.)\n", float(4096*CLOCKS_PER_SEC) / float(stop-start));


	/*
	 *
	 * Average time of the sequential insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test2));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential insert rate (ASIR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average time of the random insert
	 *
	 */
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test3));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random insert rate (ARIR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Sequential search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test4));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential search rate (ASSR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Random search
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test5));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random search rate (ARSR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average sequential delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test6));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average sequential delete rate (ASDR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Average random delete rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test7));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average random delete rate (ARDR64):      %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * Local memory 1M keys traversing time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test8));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("LSM 1M keys traversing time (LSMTT64):    %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * Average neighbours search rate
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test9));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Average neighbours search rate (ANSR64):  %.1f (ops./sec.)\n", (float)1000000 / interval);

	/*
	 *
	 * 1M dataset AND time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test10));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset AND time (ANDT64):             %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M datasets OR time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test11));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset OR time (ORT64):               %.6f (msec.)\n", 1000 * interval);
	/*
	 *
	 * 1M dataset NOT time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test12));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset NOT time (NOTT64):             %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M dataset Slice time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test13));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset slice time (SLCT64):           %.6f (msec.)\n", 1000 * interval);

	/*
	 *
	 * Worst possible insert time / Full LSM shift
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test14));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("Worst possible insert time (WPIT64):      %.3f (msec.)\n", 1000 * interval);

	/*
	 *
	 * 1M dataset squiz time
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(test15));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
	}
	interval = ((float)lnh_inst->gpc[0][LNH_CORES_LOW[0]]->mq_receive()) / (cores_count * LNH_CLOCKS_PER_SEC);
	printf("1M dataset squiz rate (SQZT64):           %.6f (sec.)\n", interval);

	/*
	 *
	 * Experiment 1
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp1));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #1. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 2
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp2));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #2. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 3
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp3));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #3. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 4
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp4));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #4. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 5
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp5));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #5. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 6
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp6));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #6. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 7
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp7));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #7. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 8
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp8));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #8. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 9
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp9));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #9. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	/*
	 *
	 * Experiment 10
	 *
	 */

	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->start_async(&__event__(exp10));
	}
	__foreach_core(group, core)
	{
		lnh_inst->gpc[group][core]->finish();
		printf("Test #10. Error count:                 	%d\n", lnh_inst->gpc[group][core]->mq_receive());
	}

	now = time(0);
	strftime(buf, 100, "Stop at local date: %d.%m.%Y.; local time: %H.%M.%S", localtime(&now));
	printf("DISC system speed test v1.1\n%s\n\n", buf);

	//--------------------------------------------------------------------------
	// Shutdown and cleanup
	//--------------------------------------------------------------------------

	clReleaseContext(context);

	if (err)
	{
		printf("ERROR: Test failed\n");
		return EXIT_FAILURE;
	}
	else
	{
		printf("INFO: Test completed successfully.\n");
		return EXIT_SUCCESS;
	}

} // end of main
