/*
 * gpc_io_swk.c
 *
 * sw_kernel library
 *
 *  Created on: April 23, 2021
 *      Author: A.Popov
 */

#include "gpc_io_swk.h"
#include "gpc_swk.h"

using namespace std;

//====================================================
// Глобальная структура очереди
//====================================================

global_memory_io gmio;

//====================================================
// Инициализация структуры
//====================================================

void gmio_init(int core)
{
    // Init queue and message offsets
    gmio.gpc2host_queue = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE;
    gmio.host2gpc_queue = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE;
    gmio.gpc2host_head_offset = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 8;
    gmio.gpc2host_tail_offset = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_QUEUE + MSG_QUEUE_FSIZE - 4;
    gmio.host2gpc_head_offset = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 8;
    gmio.host2gpc_tail_offset = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_QUEUE + MSG_QUEUE_FSIZE - 4;
    *((volatile unsigned int *)(gmio.gpc2host_head_offset)) = 0;
    *((volatile unsigned int *)(gmio.gpc2host_tail_offset)) = 0;
    *((volatile unsigned int *)(gmio.host2gpc_head_offset)) = 0;
    *((volatile unsigned int *)(gmio.host2gpc_tail_offset)) = 0;
    // Init buffers offsets
    gmio.gpc2host_buffer = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + GPC2HOST_BUFFER;
    gmio.host2gpc_buffer = AXI4BRAM_BASE + MSG_BLOCK_START + core * MSG_BLOCK_SIZE + HOST2GPC_BUFFER;
}

//====================================================
// Передача сообщения в очередь
//====================================================

void mq_send(unsigned int message)
{
    while ((*((volatile unsigned int *)(gmio.gpc2host_tail_offset)) + sizeof(unsigned int)) % MSG_QUEUE_SIZE == *((volatile unsigned int *)(gmio.gpc2host_head_offset)))
        wait;                                                                                                                                // Queue is full
    *((volatile unsigned int *)(*((volatile unsigned int *)(gmio.gpc2host_tail_offset)) + gmio.gpc2host_queue)) = message;                       // Write to the queue
    *((volatile unsigned int *)(gmio.gpc2host_tail_offset)) = (*((volatile unsigned int *)(gmio.gpc2host_tail_offset)) + sizeof(unsigned int)) % MSG_QUEUE_SIZE; // Write Tail
}

void mq_send(unsigned int size, unsigned int *buffer)
{
    for (unsigned int i=0; i<size; i++) 
        mq_send(buffer[i]);
}


//====================================================
// Чтение сообщения из очереди
//====================================================

unsigned int mq_receive()
{
    while (*((volatile unsigned int *)(gmio.host2gpc_head_offset)) == *((volatile unsigned int *)(gmio.host2gpc_tail_offset)))
        wait;                                                                                                                                // Queue is empty
    unsigned int message = *((volatile unsigned int *)(*((volatile unsigned int *)(gmio.host2gpc_head_offset)) + gmio.host2gpc_queue));          // Read the queue head
    *((volatile unsigned int *)(gmio.host2gpc_head_offset)) = (*((volatile unsigned int *)(gmio.host2gpc_head_offset)) + sizeof(unsigned int)) % MSG_QUEUE_SIZE; // Write Head
    return message;
}


//====================================================
// Чтение буфера из global memory в RAM
//====================================================

void buf_read(unsigned int size, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((volatile unsigned int *)(local_buf + i)) = *((volatile unsigned int*)(gmio.host2gpc_buffer + i));
}


//====================================================
// Чтение буфера из global memory в RAM
//====================================================

void buf_write(unsigned int size, unsigned int *local_buf)
{
    for (unsigned int i=0; i<size; i+=4) 
        *((volatile unsigned int*)(gmio.gpc2host_buffer + i)) = *((volatile unsigned int *)(local_buf + i));
}


//====================================================
// Синхронизация ядра и host через передачу сообщений
//====================================================

void sync_with_host()
{
    mq_receive();
    mq_send(0);
}
